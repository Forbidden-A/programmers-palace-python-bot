import nox


@nox.session(python=["3.8", "3.9", "3.10"])
def lint(session):
    session.install("flake8")
    session.run("flake8", "sentinel")


@nox.session
def check_format(session):
    session.install("black", "isort")
    session.run("isort", "sentinel", "--check")
    session.run("black", "sentinel", "--check")
