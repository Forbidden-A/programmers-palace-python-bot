#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# This is part of a private guild bot for Programmer's Palace
# Copyright (C) 2022  Forbidden-A

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


from tortoise import Tortoise

from sentinel import PostgresConfig

from .models import *

__all__ = ["initialise_database", "GuildData", "MessageData", "Tag", "MemberData", "UserData"]


async def initialise_database(
    config: PostgresConfig,
):
    await Tortoise.init(
        db_url=f"postgres://{config.user}:{config.password}@{config.host}:{config.port}/{config.database}",
        modules={
            "models": [
                "sentinel.database",
                "aerich.models",
            ]
        },
    )
    # Generate the schema
    await Tortoise.generate_schemas()
