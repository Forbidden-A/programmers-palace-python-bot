#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# This is part of a private guild bot for Programmer's Palace
# Copyright (C) 2022  Forbidden-A

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
import typing as t

from tortoise import Model, fields

if t.TYPE_CHECKING:
    from .guild import GuildData

__all__ = ["MessageData"]


class MessageData(Model):
    message = fields.BigIntField(pk=True, unique=True)
    author = fields.BigIntField()
    creation_datetime = fields.DatetimeField()
    guild: fields.ForeignKeyRelation["GuildData"] = fields.ForeignKeyField(
        "models.GuildData", related_name="messages", to_field="guild", on_delete="RESTRICT"
    )

    class Meta:
        table = "message"
