#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# This is part of a private guild bot for Programmer's Palace
# Copyright (C) 2022  Forbidden-A

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import typing as t

from tortoise import Model, fields

from .member import MemberData
from .tag import Tag

__all__ = ["GuildData"]


class GuildData(Model):
    guild: int = fields.BigIntField(pk=True)
    logging_channel: t.Optional[int] = fields.BigIntField(null=True)
    admin_role: t.Optional[int] = fields.BigIntField(null=True)
    mod_role: t.Optional[int] = fields.BigIntField(null=True)
    staff_role: t.Optional[int] = fields.BigIntField(null=True)
    starboard_enabled: bool = fields.BooleanField(default=False)
    starboard_threshold: int = fields.IntField(default=3)
    starboard_channel: t.Optional[int] = fields.BigIntField(null=True)
    members: fields.ReverseRelation[MemberData]
    tags: fields.ReverseRelation[Tag]

    class Meta:
        table = "guild"
