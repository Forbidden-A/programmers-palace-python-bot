from dotenv import load_dotenv

from sentinel.config import get_config

load_dotenv()
config = get_config()
postgres = config.postgres

TORTOISE_ORM = {
    "connections": {
        "default": f"postgres://{postgres.user}:{postgres.password}@{postgres.host}:{postgres.port}/{postgres.database}",
    },
    "apps": {
        "models": {
            "models": [
                "sentinel.database",
                "aerich.models",
            ],
            "default_connection": "default",
        },
    },
}
