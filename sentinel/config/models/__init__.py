#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# This is part of a private guild bot for Programmer's Palace
# Copyright (C) 2022  Forbidden-A

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
import attrs

from .base import BaseConfig
from .constants import Constants
from .discord import DiscordConfig
from .logging import LoggingConfig
from .postgresql import PostgresConfig

__all__ = ["BaseConfig", "DiscordConfig", "LoggingConfig", "PostgresConfig", "Config", "Constants"]


@attrs.define(frozen=True, auto_attribs=True)
class Config(BaseConfig):
    """
    Root Configuration class.
    """

    constants: Constants
    postgres: PostgresConfig
    logging: LoggingConfig = attrs.field(factory=LoggingConfig)
    discord: DiscordConfig = attrs.field(factory=DiscordConfig)
