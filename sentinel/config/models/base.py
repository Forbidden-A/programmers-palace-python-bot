#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# This is part of a private guild bot for Programmer's Palace
# Copyright (C) 2022  Forbidden-A

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
from __future__ import annotations

import typing as t

import attrs
import cattrs

__all__ = ["BaseConfig"]


class BaseConfig:
    @classmethod
    def from_mapping(
        cls: t.Self,
        mapping: t.Mapping[
            str,
            any,
        ],
        converter: cattrs.Converter = cattrs.global_converter,
    ) -> t.Self:
        return converter.structure(
            mapping,
            cls,
        )

    def to_dict(
        self,
    ) -> dict:
        assert attrs.has(self.__class__)
        # noinspection PyDataclass
        return attrs.asdict(self)
