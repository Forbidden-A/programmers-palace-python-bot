#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# This is part of a private guild bot for Programmer's Palace
# Copyright (C) 2022  Forbidden-A

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


"""Heavily Inspired Discord Coding Academy's Bracky Discord Bot (
https://gitlab.com/dcacademy/bracky/-/blob/master/bracky/config) """
import os
import typing as t

import toml

from .models import *

__all__ = [
    "Config",
    "PostgresConfig",
    "DiscordConfig",
    "LoggingConfig",
    "Constants",
    "deserialise_raw_config",
    "load_config_file",
    "get_config",
    "reload_config",
]

_config: t.Optional[Config] = None


def load_config_file(path: str) -> t.MutableMapping[str, t.Any]:
    """
    Loads the given config file with TOML.
    Raises an exception if the file isn't found, naturally.

    :param path: The path of the config file to load.
    :return: A MutableMapping containing configuration values.
    """
    with open(path, "r") as io:
        return toml.load(io)


def deserialise_raw_config(raw_mapping: t.Optional[t.MutableMapping[str, t.Any]] = None) -> Config:
    """
    Processes the raw mapping to the structured data models with cattrs.

    :param raw_mapping: The mapping to process.
    :return: Config instance containing processed models.
    """
    return Config.from_mapping(raw_mapping)


def get_config() -> Config:
    """
    Returns the loaded config or loads config and returns it.
    """
    global _config

    if _config is None:
        _config = deserialise_raw_config(load_config_file(os.getenv("CONFIG_FILE", "./config.toml")))
    return _config


def reload_config() -> Config:
    """
    Reloads the config and returns it.
    """
    global _config

    _config = deserialise_raw_config(load_config_file(os.getenv("CONFIG_FILE", "./config.toml")))
    return _config
