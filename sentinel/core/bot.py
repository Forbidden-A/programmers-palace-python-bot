#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# This is part of a private guild bot for Programmer's Palace
# Copyright (C) 2022  Forbidden-A

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
import logging
import os
import pathlib
import traceback

import hikari
import lightbulb
from tortoise import Tortoise

from sentinel.config import Config, get_config
from sentinel.database import GuildData, UserData, MemberData, initialise_database

__all__ = [
    "SentinelApp",
]

_LOGGER = logging.getLogger(__name__)


# noinspection PyAbstractClass
class SentinelApp(lightbulb.BotApp):
    """
    Subclass of lightbulb.BotApp to include some things.
    """

    def __init__(self, token: str, intents: hikari.Intents, **kwargs):
        self.config: Config = get_config()
        super().__init__(token, intents=intents, prefix=(self.config.discord.command_prefix,), logs=None, **kwargs)

        subscribers = {
            hikari.StartingEvent: self.on_starting,
            hikari.StoppingEvent: self.on_stopping,
            hikari.StartedEvent: self.on_started,
            hikari.ShardReadyEvent: self.on_shard_ready,
            lightbulb.CommandErrorEvent: self.on_error,
            lightbulb.events.CommandInvocationEvent: self.pre_command
        }

        for (event, callback) in subscribers.items():
            _LOGGER.debug("Subscribing with %s to %s", callback.__name__, type(event))
            self.subscribe(
                event,
                callback,
            )

    async def on_starting(self, _event: hikari.StartingEvent):
        _LOGGER.info("Bot is starting.")
        _LOGGER.info("Initialising Postgres database.")
        await initialise_database(self.config.postgres)
        _LOGGER.info("Loading extensions.")
        from sentinel import ext

        self.load_extensions_from(pathlib.Path(os.path.realpath(ext.__file__)).parent, must_exist=True, recursive=True)

    async def on_started(self, _event: hikari.StartedEvent):
        _LOGGER.info("Bot started as %s", self.get_me().__str__())

    async def on_shard_ready(self, event: hikari.ShardReadyEvent):
        _LOGGER.info("Shard %s ready as %s", event.shard.id, self.get_me().__str__())
        async for guild in self.rest.fetch_my_guilds():
            (_, new) = await GuildData.get_or_create(guild=int(guild.id))
            if new:
                _LOGGER.info("New guild: %s", str(guild.id))

    # noinspection PyMethodMayBeStatic
    async def on_stopping(self, _event: hikari.StoppingEvent):
        _LOGGER.info("Bot stopping, closing database connections.")
        await Tortoise.close_connections()

    # noinspection PyMethodMayBeStatic
    async def on_error(self, event: lightbulb.CommandErrorEvent):
        exception = event.exception.__cause__ or event.exception
        if isinstance(exception, lightbulb.CommandIsOnCooldown):
            await event.context.respond(f"Command is cooldown please wait `{exception.retry_after:.1f}s`")
            return
        else:
            _LOGGER.error(
                "Exception encountered in guild: %s :: %s: %s",
                event.context.get_guild().name,
                type(exception),
                "\n".join(traceback.format_exception(exception)),
            )

    # noinspection PyMethodMayBeStatic
    async def pre_command(self, event: lightbulb.events.CommandInvocationEvent):
        _LOGGER.info("Command %s being executed by %s", event.context.command.name, event.context.author)
        (_, new) = await UserData.get_or_create(user=int(event.context.author.id))
        if new:
            _LOGGER.info("New user: %s", str(event.context.author))
        if event.context.guild_id:
            (_, new) = await MemberData.get_or_create(user_id=int(event.context.author.id), guild_id=int(event.context.guild_id))
            if new:
                _LOGGER.info("New member: %s", str(event.context.author))
