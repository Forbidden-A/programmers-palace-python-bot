#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# This is part of a private guild bot for Programmer's Palace
# Copyright (C) 2022  Forbidden-A

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
import typing as t
from datetime import datetime, timezone
from random import randint

import hikari
import lightbulb

from sentinel import GuildData, Tag, parse_member
from sentinel.database import UserData

__all__ = ["load", "unload"]

plugin = lightbulb.Plugin(
    "Tag",
    "This plugin includes the tag command and its subcommands.",
    default_enabled_guilds=[
        896135176596377621,
    ],
)


async def tag_create(context: lightbulb.Context, name: t.Optional[str], extra: t.Optional[str]):
    """
    Create a tag in the database.
    """
    if name is None:
        await context.respond("Please specify tag name (name).")
        return
    elif extra is None:
        await context.respond("Please provide tag content (extra).")
        return

    tag = await Tag.get_or_none(name=name, guild_id=int(context.guild_id))

    if tag is not None:
        await context.respond(f"Tag `{tag.name}` already exists.")
        return

    if len(name) > 16:
        await context.respond("Name mustn't be longer than 16 characters.")
    elif len(extra) > 4096:
        await context.respond("Content too long.")
        return

    tag = await Tag.create(
        name=name,
        content=extra,
        creation_datetime=datetime.now(timezone.utc),
        owner_id=int(context.author.id),
        guild_id=int(context.guild_id),
    )
    if isinstance(context, lightbulb.SlashContext):
        await context.respond(
            f"`{tag.name}` created but you should use `!tag create name content` instead to be able to use line breaks."
        )
        return
    await context.respond(f"`{tag.name}` created.")


async def tag_delete(context: lightbulb.Context, name: t.Optional[str], _: t.Optional[str]):
    """
    Delete a tag from the database.
    """
    if name is None:
        await context.respond("Please specify which tag to delete (name).")
        return
    tag = await Tag.get_or_none(name=name, guild_id=int(context.guild_id))
    await tag.fetch_related("guild", "owner")
    if tag is None:
        await context.respond(f"No such tag `{name}`.")
        return
    guild: GuildData = tag.guild
    owner: UserData = tag.owner

    can_edit = (
        owner.user == int(context.author.id)
        or (guild.staff_role in context.member.role_ids)
        or (guild.admin_role in context.member.role_ids)
        or (guild.mod_role in context.member.role_ids)
    )

    if not can_edit:
        await context.respond(f"You aren't allowed to edit tag `{name}`.")
        return

    await tag.delete()
    await context.respond(f"`{name}` deleted.")


async def tag_edit(context: lightbulb.Context, name: t.Optional[str], extra: t.Optional[str]):
    """
    Edit a tag in the database.
    """
    if name is None:
        await context.respond("Please specify which tag to edit (name).")
        return
    tag = await Tag.get_or_none(name=name, guild_id=int(context.guild_id))
    await tag.fetch_related("guild", "owner")
    if tag is None:
        await context.respond(f"No such tag `{name}`.")
        return
    elif extra is None:
        await context.respond("Please provide new tag content (extra).")
        return
    guild: GuildData = tag.guild
    owner: UserData = tag.owner

    can_edit = (
        owner.user == int(context.author.id)
        or (guild.staff_role in context.member.role_ids)
        or (guild.admin_role in context.member.role_ids)
        or (guild.mod_role in context.member.role_ids)
    )

    if not can_edit:
        await context.respond(f"You aren't allowed to edit tag `{name}`.")
        return
    if len(extra) > 4096:
        await context.respond("Content too long.")
        return

    tag.content = extra
    await tag.save()
    if isinstance(context, lightbulb.SlashContext):
        await context.respond(
            f"`{tag.name}` edited but you should use `!tag edit name content` instead to be able to use line breaks."
        )
        return
    await context.respond(f"`{name}` edited.")


async def tag_list(context: lightbulb.Context, _n: t.Optional[str], _e: t.Optional[str]):
    """
    Get a list of tags.
    """
    guild = await GuildData.get_or_none(guild=int(context.guild_id))
    assert guild is not None
    content = "**ID** - **Name** - **Owner**"
    async for tag in guild.tags:
        await tag.fetch_related("owner")
        content += f"\n• {tag.id} - {tag.name} - <@!{tag.owner.user}>\n"
    if content == "**ID** - **Name** - **Owner**":
        await context.respond("No tags created in this guild.")
        return

    pag = lightbulb.utils.EmbedPaginator()
    pag.add_line(content)

    @pag.set_embed_factory
    def fact(index: int, cont: str):
        return hikari.Embed(title="Tag list", colour=randint(0x0, 0xFFFFFF), description=cont).set_footer(
            icon=context.member.guild_avatar_url or context.member.avatar_url or context.member.default_avatar_url,
            text=f"{index}/{len(pag)}",
        )

    nav = lightbulb.utils.ButtonNavigator(pages=pag.build_pages())
    await nav.run(context)


async def tag_info(context: lightbulb.Context, name: t.Optional[str], _: t.Optional[str]):
    """
    Get info about a certain tag.
    """
    if name is None:
        await context.respond("Please specify which tag (name).")
        return
    tag = await Tag.get_or_none(name=name, guild_id=int(context.guild_id))
    if tag is None:
        await context.respond(f"No such tag `{name}`.")
        return
    await tag.fetch_related("owner")
    embed = (
        hikari.Embed(title="Tag Information", colour=randint(0x0, 0xFFFFFF))
        .set_footer(text=f"Requested by {context.member.display_name}", icon=context.author.avatar_url)
        .add_field("Owner", f"<@!{tag.owner.user}>")
        .add_field("Uses", str(tag.uses))
    )
    await context.respond(embed)


async def tag_transfer(context: lightbulb.Context, name: t.Optional[str], extra: t.Optional[str]) -> None:
    """
    Transfer the tag to another person.
    """
    if name is None:
        await context.respond("Please specify which tag to transfer (name).")
        return
    tag = await Tag.get_or_none(name=name, guild_id=int(context.guild_id))
    await tag.fetch_related("guild")
    guild: GuildData = tag.guild
    if tag is None:
        await context.respond(f"No such tag `{name}`.")
        return
    elif extra is None:
        await context.respond("Please specify which member to transfer the tag to (extra).")
        return

    # noinspection PyUnresolvedReferences
    can_edit = (
        tag.owner_id == int(context.author.id)
        or (guild.staff_role in context.member.role_ids)
        or (guild.admin_role in context.member.role_ids)
        or (guild.mod_role in context.member.role_ids)
    )

    if not can_edit:
        await context.respond(f"You aren't allowed to transfer tag `{name}`.")
        return

    member = await parse_member(context, extra)
    if member is None:
        await context.respond(f"Unknown member '{extra}'.")
        return
    tag.owner_id = int(member.id)
    await tag.save()
    await context.respond(f"{member.mention} now owns tag `{name}`.")


async def tag_get(context: lightbulb.Context, name: str) -> None:
    """
    Get a tag from the database.
    """
    tag = await Tag.get_or_none(name=name, guild_id=int(context.guild_id))

    if tag is None:
        await context.respond(f"No such tag or action `{name}`.")
        return
    if len(tag.content) > 2000:
        pag = lightbulb.utils.StringPaginator()
        pag.add_line(tag.content)
        nav = lightbulb.utils.ButtonNavigator(pag.build_pages(), timeout=300)
        await nav.run(context)
    else:
        await context.respond(tag.content)


_SPECIAL_NAMES = {
    "create": tag_create,
    "delete": tag_delete,
    "edit": tag_edit,
    "info": tag_info,
    "list": tag_list,
    "transfer": tag_transfer,
}


@plugin.command
@lightbulb.add_cooldown(10, 3, lightbulb.UserBucket)
@lightbulb.add_checks(lightbulb.guild_only)
@lightbulb.option(
    "extra",
    "Extra argument to be passed i.e the content for the create tag command.",
    required=False,
    type=str,
    modifier=lightbulb.OptionModifier.CONSUME_REST,
)
@lightbulb.option("name", "Name of the tag to execute the action on.", required=False, type=str)
@lightbulb.option("root", "Tag name, or action to execute (create, delete, edit, info, list, transfer).", required=True, type=str)
@lightbulb.command("tag", "Cool tags!")
@lightbulb.implements(lightbulb.PrefixCommand, lightbulb.SlashCommand)
async def tag_command(context: lightbulb.Context):
    root: str = context.options.root
    name: t.Optional[str] = context.options.name
    extra: t.Optional[str] = context.options.extra
    if root not in _SPECIAL_NAMES:
        return await tag_get(context, root)

    callback = _SPECIAL_NAMES.get(root)
    assert callback is not None
    return await callback(context, name, extra)


def load(bot: lightbulb.BotApp):
    bot.add_plugin(plugin)


def unload(bot: lightbulb.BotApp):
    bot.remove_plugin("Tag")
